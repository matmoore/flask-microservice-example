Example flask web service
=========================

Deployment:

* Edit vars in ansible/site.yml
* Create ansible hosts file with server IPs
* Run ansible-playbook -i hosts site.yml

Endpoints:

* / returns a list of accounts
* /health returns the service "health"
