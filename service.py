"""
Simple web service that reads from a database and caches the most recent response.

All config comes from environment variables that are evaluated as needed.
It would be better to have a config file that gets validated when the service
starts.

The variables that must be set are
SQLALCHEMY_DATABASE_URI: URI containing database location and credentials
ACCOUNT_TABLE: the table name we are quering
ACCOUNT_COLUMN: the column name we're interested in
"""
import logging
import os
import sys
from flask import Flask, request, jsonify
from functools import wraps
from werkzeug.http import http_date
from flask_sqlalchemy import SQLAlchemy
from werkzeug.exceptions import InternalServerError, ImATeapot
from sqlalchemy.exc import DatabaseError


class SimpleHealthCheck(object):
    """
    Stores the last status code for a request to work out if we're healthy
    """
    def __init__(self):
        self.last_value = True

    def register_response(self, view):
        @wraps(view)
        def decorated(*args, **kwargs):
            try:
                result = view(*args, **kwargs)
            except Exception:
                self.last_value = False
                raise
            return result
        return decorated


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('SQLALCHEMY_DATABASE_URI', 'postgresql+psycopg2://mat:mat@localhost/micro')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
healthcheck = SimpleHealthCheck()


# I forgot to ask what these are, so I've made them configurable. Normally I would avoid this.
try:
    ACCOUNT_TABLE = os.environ['ACCOUNT_TABLE']
    ACCOUNT_COLUMN = os.environ['ACCOUNT_COLUMN']
except KeyError:
    sys.exit('Please specify ACCOUNT_TABLE and ACCOUNT_COLUMN')

def get_rows():
    return db.session.execute('select {} from {}'.format(ACCOUNT_COLUMN, ACCOUNT_TABLE))


def cache(view):
    """
    Basic memoization decorator that only uses the cached value if there is
    a database error.

    It also checks a sepcial debug param for testing purposes. For a real
    service it should look at the client's HTTP headers instead.

    We could also make this a lot more sophisticated. For example, if the
    database goes down, each new request still hits the database, which could
    make things worse. This could be avoided with the Circuit breaker pattern:
    https://en.wikipedia.org/wiki/Circuit_breaker_design_pattern
    """
    responses = {}

    @wraps(view)
    def decorated():
        cache_key = view.__name__
        resp = None

        if request.args.get('DEBUG_CACHE') != '1':
            try:
                resp = view()
            except DatabaseError:
                logging.exception('Problem communicating with database.')

        if resp is None:
            logging.warning('Returning stale data if possible.')
            cached = responses.get(cache_key)
            if cached is None:
                raise InternalServerError('Database down')
            return cached

        responses[cache_key] = resp
        resp.headers['Last-Modified'] = http_date()
        return resp

    return decorated


@app.route('/')
@healthcheck.register_response
@cache
def list_accounts():
    """
    Returns all values from the database.

    Ideally this should be paginated as the whole database must be able to fit in memory.
    """
    result = get_rows()
    return jsonify({'accounts': [row[0] for row in result]})


@app.route('/health')
def health():
    """
    This is very minimal. We probably want this to also fail if the database is down.
    """
    if healthcheck.last_value:
        return jsonify({'status': True})
    else:
        raise ImATeapot


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    logging.info('Hello world')
    app.run(debug=os.environ.get('DEBUG') == '1')
