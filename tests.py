"""
Sorry, no unit tests today.

If I were to test this I would create a basic SQlite DB with some sample data,
and use the Flask test client to check the endpoints return correct responses.

My app would need to be refactored to allow the dependencies to be injected
rather than using singletons everywhere.

I wrote it this way for simplicity, and just tested with httpie (curl replacement).

Some scenarios:

    * Two successive requests returning different data
    * 2nd request returns cached response if the table is deleted
    * Health check returning false after a failed request
    * Health check returning true initially and after a successful request
    * Error responses are returned as JSON (I didn't implement this)
"""


